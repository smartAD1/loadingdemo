package com.example.myapplication

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_two.view.*

class TwoAdapter(var list: ArrayList<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        TwoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_two, parent, false))

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.twoNum.text = list[holder.bindingAdapterPosition]
        holder.itemView.setOnClickListener {
            Toast.makeText(it.context, "${holder.absoluteAdapterPosition}", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun getItemCount(): Int = list.size
}