package com.example.myapplication

import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import io.supercharge.shimmerlayout.ShimmerLayout


object addSimmer {
    private var simmer = ArrayList<ShimmerLayout>()
    fun LinearLayout.addShimmerLayout() {
        context?.let {
            val view = View.inflate(it, R.layout.shimm_loading_item, null)
            val shimmerLayout = view.findViewById<ShimmerLayout>(R.id.shimmerLayout)
            simmer.add(shimmerLayout)
            shimmerLayout.startShimmerAnimation()
            this.addView(view,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
        }
    }

    fun LinearLayout.stopShimmerLayout() {
        simmer.forEach {
            it.stopShimmerAnimation()
        }
    }

    fun LinearLayout.addErrorLayout(listener: SetLoading) {
        simmer.forEach {
            it.stopShimmerAnimation()
            it.visibility = View.GONE
        }
        val view = View.inflate(context, R.layout.item_error, null)
        this.addView(view,
            LinearLayout.LayoutParams.MATCH_PARENT,
            100)
        view.setOnClickListener {
            removeView(view)
            MainActivity.setStatus = Status.setStatus(1)
            listener.setLoading()
        }
    }
}