package com.example.myapplication

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.toObservable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule


class MainActivity : AppCompatActivity(), SetLoading {
    var dipo = CompositeDisposable()
    companion object {
        lateinit var setStatus: Status
    }

    private val list = arrayListOf(
        ListAdapterType(arrayListOf("1", "2"), 1),
        ListAdapterType(arrayListOf("3"), 2),
        ListAdapterType(arrayListOf("4"), 3),
        ListAdapterType(arrayListOf("1"), 1)
    )
    private var adapter = ConcatAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fakeLoading()

//        val list = listOf("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
//        val listTwo = listOf("A", "B","C")
//        list.toObservable().filter {
//            it.contains("ta") || it.contains("ha")
//        }.zipWith(listTwo) { param: String, s: String ->
//            "$param and $s"
//        }.subscribe({
//            Log.d("myZipWith", it)
//        },{
//
//        })
//        val list = listOf("Alpha", "Beta", "Gamma", "Delta", "Epsilon", 0, 1)
//        val listTwo = listOf("A", "B", "C" ,"D")
//        list.toObservable().filter {
//            "$it".contains("ta") || "$it".contains("ha") || it == 0
//        }.zipWith(listTwo) { param: Any, s: String ->
//            when(param) {
//                is String -> "$param and $s"
//                is Int -> "${param +2} is Int $s"
//                else -> "$param and $s" ?: "GG"
//            }
//
//        }.subscribe({
//            Log.d("myZipWith", it)
//        }, {
//            Log.d("myZipWithError", "$it")
//        }).addTo(dipo)

//        val fragmentManager = supportFragmentManager
//        val fragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction.add(R.id.viewer, TBlankFragment.newInstance())
//        fragmentTransaction.commit()
    }


    private fun fakeLoading() {
        setStatus = Status.setStatus(1)
        setStat(setStatus)
        setStatus = Status.setStatus(3)
        loading()
    }

    private fun loading() {
        loadingView.postDelayed({
            setStat(setStatus)
        }, 5000)
    }

    fun setStat(status: Status) {
        when (status) {
            is Status.Loading -> {
                Status.Loading.loading(loadingView)
                Status.Loading.loading(loadingView)
                Status.Loading.loading(loadingView)
            }
            is Status.Finish -> {
                Status.Finish.stop(loadingView)
                loadingView.visibility = View.GONE
                val list = ArrayList<ListAdapterType>()
                repeat(1000) {
                    val type = (1..3).random()
                    list.add(ListAdapterType(arrayListOf("$it"), type))
                }
                list.forEach {
                    when (it.type) {
                        1 -> it.adapter = OneAdapter(it.list)
                        2 -> it.adapter = TwoAdapter(it.list)
                        3 -> it.adapter = ThreeAdapter(it.list)
                    }
                    it.adapter?.let {
                        adapter.addAdapter(it)
                    }
                }

                recyclerView.layoutManager = LinearLayoutManager(this)
                recyclerView.adapter = adapter
                mSwipeRefreshLayout.setOnRefreshListener {
                    Timer().schedule(3000) {
                        mSwipeRefreshLayout.isRefreshing = false
                    }
                }
            }
            is Status.Error -> {
                Status.Error.showError(loadingView, this)
                mSwipeRefreshLayout.visibility = View.GONE
            }
        }
    }

    override fun setLoading() {
        mSwipeRefreshLayout.visibility = View.VISIBLE
        setStat(setStatus)
        setStatus = Status.setStatus(2)
        loading()
    }
}

data class ListAdapterType(
    val list: ArrayList<String>,
    val type: Int,
    var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>? = null
)

interface SetLoading {
    fun setLoading()
}

//fun CompositeDisposable.addComp(disposable: CompositeDisposable) = this.add(disposable)

//@Serializable
//data class Person(val name: String ,var age: Int)