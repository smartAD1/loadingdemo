package com.example.myapplication

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment

class TBlankFragment : Fragment(),YouTubePlayer.OnInitializedListener {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance()
        val trans = childFragmentManager.beginTransaction()
        trans.add(R.id.frame_fragment,youTubePlayerFragment)
        trans.commit()
        youTubePlayerFragment.initialize("anyKey",this)
        return inflater.inflate(R.layout.fragment_t_blank, container, false)
    }


    companion object {
        fun newInstance() =
            TBlankFragment()
    }

    override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, player: YouTubePlayer?, p2: Boolean) {
        player?.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT)
        player?.cueVideo("Ht23cYaADrM")
        player?.seekRelativeMillis(100)
        player?.play()
    }

    override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
        Log.d("TBlankFragment","${p1?.name}")
        p0?.initialize("Ht23cYaADrM",this)
    }
}