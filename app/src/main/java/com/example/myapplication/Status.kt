package com.example.myapplication

import android.widget.LinearLayout
import com.example.myapplication.addSimmer.addErrorLayout
import com.example.myapplication.addSimmer.addShimmerLayout
import com.example.myapplication.addSimmer.stopShimmerLayout

sealed class Status {
    object Loading : Status() {
        fun loading(linearLayout: LinearLayout) {
            linearLayout.addShimmerLayout()
        }
    }

    object Finish : Status() {
        fun stop(linearLayout: LinearLayout) {
            linearLayout.stopShimmerLayout()
        }
    }

    object Error : Status() {
        fun showError(linearLayout: LinearLayout, listener: SetLoading) {
            linearLayout.addErrorLayout(listener)
        }
    }

    companion object {
        fun setStatus(num: Int): Status {
            return when (num) {
                1 -> Loading
                2 -> Finish
                3 -> Error
                else -> Error
            }
        }
    }
}